from starlette.middleware.base import BaseHTTPMiddleware
from api_demo.logger import logger


class LogMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        response = await call_next(request)
        extra={
            "client_host": request.client.host,
            "client_port": str(request.client.port),
            "method": request.method,
            "url": str(request.url),
            "status_code": response.status_code,
        }
        logger.info(
            f"client={request.client.host} method={request.method} url={str(request.url)} status={response.status_code}",
            extra=extra,
        )
        return response
