import os
import json
import logging
from logging import Formatter

class JsonFormatter(Formatter):

    allowed_keywords = [
        'client_host',
        'client_port',
        'method',
        'url',
        'status_code'
    ]

    def __init__(self):
        super(JsonFormatter, self).__init__()

    def format(self, record):
        json_record = {}
        json_record["message"] = record.getMessage()
        for k in self.allowed_keywords:
            if k in record.__dict__:
                json_record[k] = record.__dict__[k]
        if record.levelno == logging.ERROR and record.exc_info:
            json_record['err'] = self.formatException(record.exc_info)

        return json.dumps(json_record)

logger = logging.root
handler = logging.StreamHandler()
handler.setFormatter(JsonFormatter())
logger.handlers = [handler]
logger.setLevel(logging.DEBUG)

if 'API_DEMO_LOGGER' in os.environ:
    logging.getLogger("uvicorn.access").disabled = True

