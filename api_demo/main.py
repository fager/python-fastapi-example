import os
from fastapi import FastAPI, Request
from fastapi.responses import PlainTextResponse
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse
from prometheus_fastapi_instrumentator import Instrumentator
from pydantic import BaseModel, Field
from api_demo.logger import logger
from api_demo.log_middleware import LogMiddleware
import uvicorn
import uuid

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

if 'API_DEMO_LOGGER' in os.environ:
    app.add_middleware(LogMiddleware)

class Item(BaseModel):
    """Prise list item"""
    name: str
    price: float


class ResponseMessage(BaseModel):
    """Simple Text Message Response"""
    message: str
    uuid: str = Field(default_factory=uuid.uuid4, title="unique respone ID")

class EchoResponseMessage(BaseModel):
    """Response Message for echo-Method"""
    method: str = Field(None, title="Request Method")
    path: str = Field(None, title="Request Path")
    args: str
    headers: dict
    uuid: str = Field(default_factory=uuid.uuid4, title="unique respone ID")

class StatusCodeMessage(BaseModel):
    """Response Message for status endpoint requests"""
    code: int = Field(None, title="HTTP Status Code")
    message: str = Field(None, title="Status Message")

class ApiInfoMessage(BaseModel):
    """Response Message for api-info endpoint requests"""
    description: str = Field(None, title="API Description")
    nonce: str = Field(default_factory=uuid.uuid4, title="Nonce")

@app.on_event("startup")
async def startup():
    Instrumentator().instrument(app).expose(app)

@app.post("/items/", response_model=ResponseMessage)
async def create_item(item: Item):
    return {"message": "item received"}


@app.get("/items/", response_model=list[Item])
async def get_items():
    return [
        {"name": "Plumbus", "price": 3},
        {"name": "Portal Gun", "price": 9001},
    ]

@app.get("/status/{code}", response_model=StatusCodeMessage, status_code=200)
async def status(code: int):
    doc = {
        "code": code,
        "message": f"You requested a {code} response"
    }
    res = JSONResponse(doc)
    res.status_code = code
    return res

@app.get("/health", response_class=PlainTextResponse)
async def healthcheck():
    return "200"

@app.get("/foo")
async def foo() -> ResponseMessage:
    return ResponseMessage(message="foo")

@app.get("/bar")
async def foo() -> ResponseMessage:
    return ResponseMessage(message="bar")

@app.get("/api-info")
async def api_info(request: Request) -> ApiInfoMessage:
    description = "API Demo"
    if 'x-3scale-proxy-secret-token' in request.headers:
        psk = request.headers['x-3scale-proxy-secret-token']
        psk = psk[-4:]
        description = f"API Demo (psk: {psk})"
    else:
        description="API Demo (without PSK)"
    return ApiInfoMessage(
        description=description
    )


@app.get("/echo", response_model=EchoResponseMessage)
async def echo(request: Request):
    return {
        "method": request.method,
        "path": request.url.path,
        "args":  str(request.query_params),
        "headers": request.headers,
        "uuid": str(uuid.uuid4())
    }
if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=8080, log_config=None)

