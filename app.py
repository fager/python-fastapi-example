import asyncio
import uvicorn
from api_demo.logger import logger

async def main():
    config = uvicorn.Config(
        'api_demo.main:app',
        host='0.0.0.0',
        port=8080,
        log_config=None,
        workers=8,
    )
    server = uvicorn.Server(config)
    await server.serve()

if __name__ == '__main__':
    asyncio.run(main())

